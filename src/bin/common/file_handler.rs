use std::path::Path;

pub fn open_input_file() -> Result<String, i32> {
    let argv_first = match std::env::args().next() {
        None => { log::error!("Could not get first element of argv"); return Err(1) }
        Some(data) => data
    };

    let bin_name = match Path::new(&argv_first).file_name() {
        None => { log::error!("Could not get binary file name from argv"); return Err(1) }
        Some(bin_name) => { match bin_name.to_str() {
                None => { log::error!("Could not parse binary name to str"); return Err(1) }
                Some(bin_name_str) => bin_name_str
            }
        }
    };

    let full_path = std::format!("{}/data/{}/input", env!("CARGO_MANIFEST_DIR"), bin_name);
    match std::fs::read_to_string(full_path) {
        Ok(data) => Ok(data),
        Err(err) => {
            log::error!("Could not open file: {}", err.to_string());
            Err(1)
        }
    }
}
