pub fn init_logger() -> Result<(), i32> {
    match simple_logger::SimpleLogger::new().env().init() {
        Ok(_) => {
            log::info!("Logger initiated");
            Ok(())
        },
        Err(err) => {
            println!("Could not init logger: {}", err.to_string());
            Err(1)
        }
    }
}