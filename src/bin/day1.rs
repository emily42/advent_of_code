mod common;

#[derive(Clone, Debug)]
struct Elf {
    pub id: usize,
    snacks: Vec<u32>,
    sum: u32
}

impl Elf {
    pub fn new(id: usize) -> Self {
        Self {
            id,
            snacks: Vec::new(),
            sum: 0
        }
    }
}

fn parse_elves_data(input: String) -> Result<Vec<Elf>, i32> {
    let mut elves_vec = Vec::new();
    let mut elf = Elf::new(elves_vec.len());
    input.split("\n\n");
    for line in input.lines() {
        if line.is_empty() {
            // Calculate sum
            elf.sum = elf.snacks.iter().sum();
            log::info!("Registered elf {} with a callorie count of {}", elf.id, elf.sum);

            // Reload a new elf
            elves_vec.push(elf);
            elf = Elf::new(elves_vec.len());
            continue;
        }

        match str::parse::<u32>(line) {
            Ok(paresed_line)=> {
                elf.snacks.push(paresed_line);
            },
            Err(err) => {
                log::error!("Could not parse number '{}', make sure your input file is valid: {}", line, err.to_string());
                return Err(1);
            }
        }
    }
    // Sort by calorie count
    elves_vec.sort_by(|a, b|
        a.sum
            .cmp(&b.sum)
            .reverse()
    );
    Ok(elves_vec)
}

fn part_one(elves_vec: &Vec<Elf>) -> Result<(), i32> {
    let thiccest_elf = match elves_vec.iter().nth(0) {
        None => Err(1),
        Some(elf) => Ok(elf)
    }?;
    log::info!("The THICCest elf was definitely {}, with a calorie sum of {}", thiccest_elf.id, thiccest_elf.sum);
    Ok(())
}

fn part_two(elves_vec: &Vec<Elf>) -> Result<(), i32> {
    let three_thiccest_elves_sum = &elves_vec[..3]
        .iter()
        .fold(Ok(0), |acc: Result<u32, i32>, next| Ok(acc? + next.sum))?;
    log::info!("The sum of calories for three thiccest elves is {}", three_thiccest_elves_sum);
}

pub fn main() -> Result<(), i32> {
    common::logging::init_logger()?;
    let input = common::file_handler::open_input_file()?;
    let elves_vec = parse_elves_data(input)?;

    part_one(&elves_vec)?;
    part_two(&elves_vec)?;

    Ok(())
}
