mod common;

#[derive(Debug, Copy, Clone)]
enum RPSEnum {
    ROCK = 1,
    PAPER = 2,
    SCISSORS = 3
}

fn parse_to_points_enum(input_letter: &char) -> Result<RPSEnum, i32> {
    match input_letter {
        'A' | 'X' => Ok(RPSEnum::ROCK),
        'B' | 'Y' => Ok(RPSEnum::PAPER),
        'C' | 'Z' => Ok(RPSEnum::SCISSORS),
        _ => {
            log::info!("Received an unexpected input, aborting");
            Err(1)
        }
    }
}

#[derive(Debug, Copy, Clone)]
enum RPSResultEnum {
    LOSS = 0,
    TIE = 3,
    VICTORY = 6
}

fn parse_to_result_enum(input_letter: &char) -> Result<RPSResultEnum, i32> {
    match input_letter {
        'X' => Ok(RPSResultEnum::LOSS),
        'Y' => Ok(RPSResultEnum::TIE),
        'Z' => Ok(RPSResultEnum::VICTORY),
        _ => {
            log::info!("Received an unexpected input, aborting");
            Err(1)
        }
    }
}

fn calculate_acquired_points(my_choice: &RPSEnum, opponent_choice: &RPSEnum) -> u8 {
    *my_choice as u8 + match (my_choice, opponent_choice) {
        // Loss
        (RPSEnum::ROCK, RPSEnum::PAPER) |
        (RPSEnum::PAPER, RPSEnum::SCISSORS) |
        (RPSEnum::SCISSORS, RPSEnum::ROCK) => RPSResultEnum::LOSS,

        // Tie
        (RPSEnum::ROCK, RPSEnum::ROCK) |
            (RPSEnum::PAPER, RPSEnum::PAPER) |
                (RPSEnum::SCISSORS, RPSEnum::SCISSORS) => RPSResultEnum::TIE,

        // Victory
        (RPSEnum::ROCK, RPSEnum::SCISSORS) |
            (RPSEnum::PAPER, RPSEnum::ROCK) |
                (RPSEnum::SCISSORS, RPSEnum::PAPER) => RPSResultEnum::VICTORY,
    } as u8
}

fn calculate_move(my_choice: &char, opponent_choice: &RPSEnum) -> Result<RPSEnum, i32> {
    let my_needed_choice = parse_to_result_enum(my_choice)?;
    Ok(match (opponent_choice, my_needed_choice) {
        (RPSEnum::PAPER, RPSResultEnum::LOSS) |
            (RPSEnum::ROCK, RPSResultEnum::TIE) |
                (RPSEnum::SCISSORS, RPSResultEnum::VICTORY) => RPSEnum::ROCK,

        (RPSEnum::SCISSORS, RPSResultEnum::LOSS) |
            (RPSEnum::PAPER, RPSResultEnum::TIE) |
            (RPSEnum::ROCK, RPSResultEnum::VICTORY) => RPSEnum::PAPER,

        (RPSEnum::ROCK, RPSResultEnum::LOSS) |
            (RPSEnum::SCISSORS, RPSResultEnum::TIE) |
                (RPSEnum::PAPER, RPSResultEnum::VICTORY) => RPSEnum::SCISSORS,
    })
}

#[derive(Debug, Copy, Clone)]
struct MatchResults {
    _my_choice: RPSEnum,
    _opponent_choice: RPSEnum,
    points_acquired: u8
}

impl MatchResults {
    pub fn new(my_choice: RPSEnum, opponent_choice: RPSEnum) -> Result<Self, i32> {
        let points_acquired = calculate_acquired_points(&my_choice, &opponent_choice);
        Ok(Self {
            _my_choice : my_choice,
            _opponent_choice : opponent_choice,
            points_acquired
        })
    }

    pub fn points_acquired(&self) -> u8 {
        self.points_acquired
    }
}

fn sum_match_results(current_acc: Result<u32, i32>, maybe_match_result: Result<MatchResults, i32>) -> Result<u32, i32> {
    Ok(current_acc? + maybe_match_result?.points_acquired() as u32)
}

fn part_one(input: &String) -> Result<(), i32> {
    let match_results = input.lines()
        .map(|line| {
            // Empty chars will still be handled in the func
            MatchResults::new(
                parse_to_points_enum(&line.chars().nth(2).unwrap_or(' '))?,
                parse_to_points_enum(&line.chars().nth(0).unwrap_or(' '))?
            )
        }).fold(Ok(0_u32), sum_match_results)?;

    log::info!("Part one: I got a total amount of {} points", match_results);
    Ok(())
}

fn part_two(input: &String) -> Result<(), i32> {
    let match_results = input.lines()
        .map(|line| {
            // Empty chars will still be handled in the func;
            let opponent_choice = parse_to_points_enum(&line.chars().nth(0).unwrap_or(' '))?;
            let my_choice = calculate_move(&line.chars().nth(2).unwrap_or(' '), &opponent_choice)?;

            MatchResults::new(
                my_choice,
                opponent_choice
            )
        }).fold(Ok(0_u32), sum_match_results)?;

    log::info!("Part two: I got a total amount of {} points", match_results);
    Ok(())
}

pub fn main() -> Result<(), i32> {
    common::logging::init_logger()?;
    let input = common::file_handler::open_input_file()?;

    part_one(&input)?;
    part_two(&input)?;

    Ok(())
}
