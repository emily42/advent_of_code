use std::{collections::{HashSet}};

mod common;

const ITEM_PRIORITIES: [char; 52] = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
                                        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];                                        

#[derive(Debug, Clone)]
struct Backpack {
    _id: u32,
    both_compartments: HashSet<char>,
    left_compartment: HashSet<char>,
    right_compartment: HashSet<char>
}

impl Backpack {
    pub fn new(current_idx: &mut u32, line: &str) -> Result<Self, i32> {
        if line.len() % 2 != 0 {
            log::error!("Can't split line containing compartments, does not contain an even number of chars");
            return Err(1);
        }
        
        let id = current_idx.clone();
        *current_idx += 1;

        let (left_comparment, right_compartment) = line.split_at(line.len()/2);
        Ok(Self {
            _id: id,
            both_compartments: HashSet::from_iter(line.chars()),
            left_compartment: HashSet::from_iter(left_comparment.chars()),
            right_compartment: HashSet::from_iter(right_compartment.chars()),
        })
    }

    pub fn intersection(&self) -> Result<&char, i32> {
        match self.left_compartment.intersection(&self.right_compartment).next() {
            None => Err(1),
            Some(intersection) => Ok(intersection)
        }
    }

    pub fn backpack_intersection(&self, hashset: &HashSet<char>) -> HashSet<char> {
        hashset
            .intersection(&self.both_compartments)
                .map(|a| a
                    .clone())
                        .collect::<HashSet<char>>()
    }
}

fn parse_backpacks(input: &String) -> Result<Vec<Backpack>, i32> {
    let mut idx = 0;
    Ok(
        input.lines().map(|line| {
            Backpack::new(&mut idx, line)
                })
                    .collect::<Result<Vec<Backpack>, i32>>()?
    )
}

fn get_item_priority(item: &char) -> Result<u32, i32> {
    match ITEM_PRIORITIES.iter().position(|x| x == item) {
        Some(priorirty) => Ok(priorirty as u32 +1),
        None => Err(1),
    }
}

fn sum_backpack_group(group: &[Backpack]) -> Result<u32, i32> {
    let backpack_group_intersection = match group.iter()
            .fold(HashSet::from_iter(ITEM_PRIORITIES), |acc: HashSet<char>, next| {
                next.backpack_intersection(&acc)
            })
                .iter()
                    .next() {
                        None => {
                            log::error!("No intersection in backpack group");
                            Err(1)
                        },
                        Some(val) => get_item_priority(val)
                    };

    backpack_group_intersection
}

fn part_one(all_backpacks: &Vec<Backpack>) -> Result<(), i32> {
    let sum_of_shared_objects_in_backpacks = all_backpacks
        .iter()
            .map(|backpack: &Backpack| Ok(backpack.intersection()?))
                .fold(Ok(0), |acc: Result<u32, i32> , item: Result<&char, i32>| {
                    Ok(acc? + get_item_priority(item?)?)
                })?;

    log::info!("The sum of priorities of all items recurring in both compartments is {}", sum_of_shared_objects_in_backpacks);
    Ok(())
}

fn part_two(all_backpacks: &Vec<Backpack>) -> Result<(), i32> {
    let sum_of_badges = all_backpacks
        .chunks(3)
            .map(|backpack_group: &[Backpack]| {
                sum_backpack_group(&backpack_group)
            }).fold(Ok(0_u32), |acc: Result<u32, i32>, next| Ok(acc? + next?))?;

    log::info!("Sum of all group badges is {}", sum_of_badges);
    Ok(())
}

pub fn main() -> Result<(), i32> {
    common::logging::init_logger()?;
    let input = common::file_handler::open_input_file()?;
    let all_backpacks = parse_backpacks(&input)?;

    part_one(&all_backpacks)?;
    part_two(&all_backpacks)?;

    Ok(())
}
